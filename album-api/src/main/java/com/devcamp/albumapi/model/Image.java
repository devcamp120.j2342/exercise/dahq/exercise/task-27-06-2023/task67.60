package com.devcamp.albumapi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "images")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "Nhập ma image")
    @Size(min = 3, message = "Mã image phải có ít nhất 2 ký tự ")
    @Column(name = "image_code", unique = true, nullable = false)
    private String imageCode;
    @Column(name = "image_name")
    private String imageName;
    @Column(name = "url_image")
    private String urlImage;
    @Column(name = "descript_image")
    private String descriptImage;
    @Column(name = "create_image")
    private Date createImage;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "album_id", nullable = false)
    private Album album;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImageCode() {
        return imageCode;
    }

    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getDescriptImage() {
        return descriptImage;
    }

    public void setDescriptImage(String descriptImage) {
        this.descriptImage = descriptImage;
    }

    public Date getCreateImage() {
        return createImage;
    }

    public void setCreateImage(Date createImage) {
        this.createImage = createImage;
    }

    public Image(long id, String imageCode, String imageName, String urlImage, String descriptImage, Date createImage) {
        this.id = id;
        this.imageCode = imageCode;
        this.imageName = imageName;
        this.urlImage = urlImage;
        this.descriptImage = descriptImage;
        this.createImage = createImage;
    }

    public Image() {
    }

    // public Album getAlbum() {
    // return album;
    // }

    public void setAlbum(Album album) {
        this.album = album;
    }

}
