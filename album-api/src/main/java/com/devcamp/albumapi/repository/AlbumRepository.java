package com.devcamp.albumapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.albumapi.model.Album;

public interface AlbumRepository extends JpaRepository<Album, Long> {

}
