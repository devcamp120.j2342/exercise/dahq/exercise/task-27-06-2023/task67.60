package com.devcamp.albumapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.albumapi.model.Image;

public interface ImageRepository extends JpaRepository<Image, Long> {

}
