package com.devcamp.albumapi.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.albumapi.model.Album;
import com.devcamp.albumapi.model.Image;
import com.devcamp.albumapi.repository.AlbumRepository;
import com.devcamp.albumapi.repository.ImageRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ImageController {
    @Autowired
    ImageRepository imageRepository;
    @Autowired
    AlbumRepository albumRepository;

    @PostMapping("/image/create/{id}")
    public ResponseEntity<Image> createImage(@Valid @PathVariable("id") long id, @RequestBody Image pImage) {
        Optional<Album> cAlbum = albumRepository.findById(id);
        if (cAlbum.isPresent()) {
            try {
                Image cImage = new Image();
                cImage.setImageCode(pImage.getImageCode());
                cImage.setImageName(pImage.getImageName());
                cImage.setUrlImage(pImage.getUrlImage());
                cImage.setCreateImage(new Date());
                cImage.setDescriptImage(pImage.getDescriptImage());
                cImage.setAlbum(cAlbum.get());
                return new ResponseEntity<>(imageRepository.save(cImage), HttpStatus.CREATED);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/image/update/{id}")
    public ResponseEntity<Image> updateImage(@PathVariable("id") long id, @RequestBody Image pImage) {
        Optional<Image> cImage = imageRepository.findById(id);
        if (cImage.isPresent()) {
            try {
                cImage.get().setImageCode(pImage.getImageCode());
                cImage.get().setImageName(pImage.getImageName());
                cImage.get().setDescriptImage(pImage.getDescriptImage());
                cImage.get().setUrlImage(pImage.getUrlImage());
                return new ResponseEntity<Image>(imageRepository.save(cImage.get()), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/image/delete/{id}")
    public ResponseEntity<Image> deleteImage(@PathVariable("id") long id) {
        imageRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/image/all")
    public List<Image> getAllImage() {
        return imageRepository.findAll();
    }

    @GetMapping("/image/details/{id}")
    public Image getImageById(@PathVariable("id") long id) {
        return imageRepository.findById(id).get();
    }

}
